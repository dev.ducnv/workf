package request

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"shopf1/go-flow/entities"
)

type QueryListRequest struct {
	ShopId   int `json:"shop_id" schema:"shop_id"`
	PageSize int `json:"page_size" schema:"page_size"`
	Page     int `json:"page" schema:"page"`
}

func QueryListValidation(request QueryListRequest) string {
	//err := validation.ValidateStruct(&request))
	//if err != nil {
	//	return err.Error()
	//}
	return ""
}

func CreatePartnerValidation(request entities.Partners) string {
	err := validation.ValidateStruct(&request, validation.Field(&request.ShopId, validation.Required))
	if err != nil {
		return err.Error()
	}
	return ""
}

func UpdatePartnerValidation(request entities.Partners) string {
	err := validation.ValidateStruct(&request, validation.Field(&request.ShopId, validation.Required),
		validation.Field(&request.ID, validation.Required),
	)
	if err != nil {
		return err.Error()
	}
	return ""
}

func DeletePartnerValidation(request entities.Partners) string {
	err := validation.ValidateStruct(&request, validation.Field(&request.ShopId, validation.Required),
		validation.Field(&request.ID, validation.Required),
	)
	if err != nil {
		return err.Error()
	}
	return ""
}

type LandingPageRequest struct {
	ShopId   int                    `json:"shop_id" schema:"shop_id"`
	EventId  int                    `json:"event_id" schema:"event_id"`
	Guardian entities.ShopCustomers `json:"guardian" schema:"guardian"`
	Tickets  []entities.Ticket      `json:"tickets" schema:"tickets"`
}
