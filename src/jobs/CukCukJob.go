package jobs

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"partner-sync/src/models"
	"partner-sync/src/response"
	"shopf1/go-flow/constants"
	"shopf1/go-flow/database"
	"shopf1/go-flow/entities"
	"shopf1/go-flow/grpc/TriggerService"
	"shopf1/go-flow/utils"
	"time"
)

func SendTriggerToOneByLastCare(lastCare int, shopId int, customerId int) {
	triggerList := entities.GetTriggerConfigRedis(&shopId)
	var triggers []entities.Trigger
	for _, trig := range triggerList.Triggers {
		if trig.Type == constants.TriggerTypeFieldUpdate {
			for _, condition := range trig.CustomerCondition.TriggerComparisons {
				if condition.FieldType == constants.CareStatus && condition.Value == utils.IntToString(lastCare) {
					triggers = append(triggers, trig)
					break
				}
			}
		}
	}
	if len(triggers) > 0 {
		for i := 0; i < len(triggers); i++ {
			TriggerService.SendTriggerToOne(triggers[i].ID, "", int64(customerId))
		}

	}

}

func HandlerCukCuk(partner entities.Partners) {
	loginRequest := make(map[string]string)
	loginRequest["AppID"] = partner.AppId
	loginRequest["Domain"] = partner.Domain
	loginRequest["LoginTime"] = time.Now().UTC().Format(time.RFC3339)

	jsonStr, err := json.Marshal(loginRequest)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(string(jsonStr))

	h := hmac.New(sha256.New, []byte(partner.Secret))

	h.Write(jsonStr)

	signatureInfo := hex.EncodeToString(h.Sum(nil))
	//fmt.Println("Result: " + signatureInfo)
	loginRequest["SignatureInfo"] = signatureInfo

	jsonStr, err = json.Marshal(loginRequest)
	if err != nil {
		fmt.Println(err)
	}

	//fmt.Println(string(jsonStr))

	req, err := http.NewRequest("POST", "https://graphapi.cukcuk.vn/api/Account/Login", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	var resLogin = response.CukCukResponse{}

	errRes := json.Unmarshal(body, &resLogin)
	if errRes != nil {
		fmt.Println(errRes)
		return
	}

	if resLogin.Data.AccessToken == "" {
		return
	}

	// Đồng bộ khách hàng từ CukCuk

	var canLoadMoreCustomer = true
	var currentLastSync = time.Now().UTC()
	var page = 0
	//
	for canLoadMoreCustomer == true {
		page += 1
		customerRequest := make(map[string]interface{})
		customerRequest["Page"] = page
		customerRequest["Limit"] = 100
		customerRequest["LastSyncDate"] = partner.LastSync.UTC().Format(time.RFC3339)

		jsonStr, err = json.Marshal(customerRequest)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(string(jsonStr))

		req, err = http.NewRequest("POST", "https://graphapi.cukcuk.vn/api/v1/customers/paging", bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Authorization", "Bearer "+resLogin.Data.AccessToken)
		req.Header.Set("CompanyCode", resLogin.Data.CompanyCode)
		resp, err = client.Do(req)
		if err != nil {
			return
		}
		defer resp.Body.Close()

		body, _ = io.ReadAll(resp.Body)
		//fmt.Println("response Body:", string(body))

		var resCustomer = response.CukCukCustomerResponse{}

		errRes = json.Unmarshal(body, &resCustomer)
		if errRes != nil {
			fmt.Println(errRes)
			return
		}

		if len(resCustomer.Data) > 0 {
			canLoadMoreCustomer = true
		} else {
			canLoadMoreCustomer = false
		}

		for i := 0; i < len(resCustomer.Data); i++ {

			var cukcukCustomer = entities.CukCukCustomers{}
			database.DB.Model(&entities.CukCukCustomers{}).Where(&entities.CukCukCustomers{ShopId: partner.ShopId, ID: resCustomer.Data[i].Id}).First(&cukcukCustomer)
			if cukcukCustomer.ID == "" {
				var shopCustomer = entities.ShopCustomers{}

				var lastCare = 0
				if resCustomer.Data[i].TotalAmount > 0 {
					lastCare = partner.LastCare
				}

				database.DB.Model(&entities.ShopCustomers{}).Where("shop_id", partner.ShopId).Where("phone", resCustomer.Data[i].Tel).Scan(&shopCustomer)
				if shopCustomer.ID == 0 {
					shopCustomer = entities.ShopCustomers{
						ShopID:      partner.ShopId,
						SocialType:  "cukcuk",
						FullName:    resCustomer.Data[i].Name,
						Phone:       resCustomer.Data[i].Tel,
						AddressFull: resCustomer.Data[i].Address,
						Birth:       resCustomer.Data[i].Birthday,
						LastCare:    lastCare,
					}
					database.DB.Model(&entities.ShopCustomers{}).Create(&shopCustomer)
				} else {
					database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ShopID: partner.ShopId, ID: shopCustomer.ID}).Updates(&entities.ShopCustomers{LastCare: partner.LastCare})
					go func() {
						defer func() {
							if err := recover(); err != nil {
								fmt.Sprintf("panic occurred")
							}
						}()

						SendTriggerToOneByLastCare(partner.LastCare, partner.ShopId, shopCustomer.ID)
					}()
				}

				database.DB.Model(&entities.CukCukCustomers{}).Create(&entities.CukCukCustomers{
					ShopId:         partner.ShopId,
					ID:             resCustomer.Data[i].Id,
					ShopCustomerId: shopCustomer.ID,
					Code:           resCustomer.Data[i].Code,
					TotalAmount:    int(resCustomer.Data[i].TotalAmount),
				})
			} else if int(resCustomer.Data[i].TotalAmount) != cukcukCustomer.TotalAmount {

				database.DB.Model(&entities.CukCukCustomers{}).Where(&entities.CukCukCustomers{ID: cukcukCustomer.ID}).Updates(&entities.CukCukCustomers{
					TotalAmount: int(resCustomer.Data[i].TotalAmount),
				})
				if cukcukCustomer.TotalAmount == 0 {
					database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ShopID: partner.ShopId, ID: cukcukCustomer.ShopCustomerId}).Updates(&entities.ShopCustomers{
						LastCare: partner.LastCare,
					})
				}
			}

		}
	}

	// Lấy tất cả cửa hàng

	req, err = http.NewRequest("GET", "https://graphapi.cukcuk.vn/api/v1/branchs/all", nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+resLogin.Data.AccessToken)
	req.Header.Set("CompanyCode", resLogin.Data.CompanyCode)
	resp, err = client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, _ = io.ReadAll(resp.Body)
	//fmt.Println("response Body:", string(body))

	var resBranch = response.CukCukBranchResponse{}

	errRes = json.Unmarshal(body, &resBranch)
	if errRes != nil {
		fmt.Println(errRes)
		return
	}

	var canLoadMoreOrder = true
	page = 0

	for canLoadMoreOrder == true {
		page += 1
		orderRequest := make(map[string]interface{})
		orderRequest["Page"] = page
		orderRequest["Limit"] = 100
		orderRequest["LastSyncDate"] = partner.LastSync.UTC().Format(time.RFC3339)

		jsonStr, err = json.Marshal(orderRequest)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Println(string(jsonStr))

		req, err = http.NewRequest("POST", "https://graphapi.cukcuk.vn/api/v1/orders/paging", bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Authorization", "Bearer "+resLogin.Data.AccessToken)
		req.Header.Set("CompanyCode", resLogin.Data.CompanyCode)
		resp, err = client.Do(req)
		if err != nil {
			return
		}
		defer resp.Body.Close()

		body, _ = io.ReadAll(resp.Body)
		//fmt.Println("response Body:", string(body))

		var resOrder = response.CukCukOrderResponse{}

		errRes = json.Unmarshal(body, &resOrder)

		for i := 0; i < len(resOrder.Data); i++ {

			if resOrder.Data[i].CustomerId == "" {
				continue
			}

			var branch = ""
			for j := 0; j < len(resBranch.Data); j++ {
				if resBranch.Data[j].Id == resOrder.Data[i].BranchId {
					branch = resBranch.Data[j].Name
				}
			}

			var order = entities.Orders{}

			database.DB.Model(&entities.Orders{}).Where(&entities.Orders{ShippingCode: resOrder.Data[i].Id}).First(&order)
			if order.ID == 0 {
				var cukcukCustomer = entities.CukCukCustomers{}
				database.DB.Model(&entities.CukCukCustomers{}).Where(&entities.CukCukCustomers{ShopId: partner.ShopId, ID: resOrder.Data[i].CustomerId}).First(&cukcukCustomer)
				if cukcukCustomer.ID == "" {

					var customer = entities.ShopCustomers{}

					database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ShopID: partner.ShopId, Phone: resOrder.Data[i].CustomerTel}).First(&customer)

					if customer.ID == 0 {
						customer = entities.ShopCustomers{
							ShopID:     partner.ShopId,
							SocialType: "cukcuk",
							FullName:   resOrder.Data[i].CustomerName,
							Phone:      resOrder.Data[i].CustomerTel,
							LastCare:   partner.LastCare,
						}
						database.DB.Model(&entities.ShopCustomers{}).Create(&customer)
						go func() {
							defer func() {
								if err := recover(); err != nil {
									fmt.Sprintf("panic occurred")
								}
							}()

							SendTriggerToOneByLastCare(partner.LastCare, partner.ShopId, customer.ID)
						}()
					} else if customer.LastCare != partner.LastCare {
						database.DB.Model(&entities.ShopCustomers{}).Where(entities.ShopCustomers{ShopID: partner.ShopId, ID: customer.ID}).Updates(&entities.ShopCustomers{
							LastCare: partner.LastCare,
						})
						go func() {
							defer func() {
								if err := recover(); err != nil {
									fmt.Sprintf("panic occurred")
								}
							}()

							SendTriggerToOneByLastCare(partner.LastCare, partner.ShopId, customer.ID)
						}()
					}

					cukcukCustomer = entities.CukCukCustomers{
						ShopId:         partner.ShopId,
						ID:             resOrder.Data[i].CustomerId,
						ShopCustomerId: customer.ID,
						TotalAmount:    int(resOrder.Data[i].TotalAmount),
					}

					database.DB.Model(&entities.CukCukCustomers{}).Create(&cukcukCustomer)

					var order = models.CreateOrderRequest{
						ShopID:          partner.ShopId,
						ShippingName:    customer.FullName,
						ShippingPhone:   customer.Phone,
						ShippingAddress: customer.Address,
						Total:           int(resOrder.Data[i].TotalAmount * 1.1),
						CustomerID:      cukcukCustomer.ShopCustomerId,
						ShippingCode:    resOrder.Data[i].Id,
						OrderNumber:     fmt.Sprintf(`IDO%d`, time.Now().UnixMicro()),
						Detail:          branch,
						CreatedAt:       resOrder.Data[i].Date,
						Status:          4,
					}
					database.DB.Model(&entities.Orders{}).Create(&order)

				} else {

					var customer = entities.ShopCustomers{}
					database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ID: cukcukCustomer.ShopCustomerId}).First(&customer)

					var order = models.CreateOrderRequest{
						ShopID:          partner.ShopId,
						ShippingName:    customer.FullName,
						ShippingPhone:   customer.Phone,
						ShippingAddress: customer.Address,
						Total:           int(resOrder.Data[i].TotalAmount * 1.1),
						CustomerID:      cukcukCustomer.ShopCustomerId,
						ShippingCode:    resOrder.Data[i].Id,
						OrderNumber:     fmt.Sprintf(`IDO%d`, time.Now().UnixMicro()),
						Detail:          branch,
						CreatedAt:       resOrder.Data[i].Date,
						Status:          4,
					}
					database.DB.Model(&entities.Orders{}).Create(&order)

					database.DB.Model(&entities.CukCukCustomers{}).Where(&entities.CukCukCustomers{ID: cukcukCustomer.ID}).Updates(&entities.CukCukCustomers{
						TotalAmount: cukcukCustomer.TotalAmount + int(resOrder.Data[i].TotalAmount*1.1),
					})
					if cukcukCustomer.TotalAmount == 0 && customer.LastCare != partner.LastCare {
						database.DB.Model(&entities.ShopCustomers{}).Where(entities.ShopCustomers{ShopID: partner.ShopId, ID: customer.ID}).Updates(&entities.ShopCustomers{
							LastCare: partner.LastCare,
						})
					}

				}
			}
		}
		if len(resOrder.Data) > 0 {
			canLoadMoreOrder = true
		} else {
			canLoadMoreOrder = false
		}

	}

	database.DB.Model(&entities.Partners{}).Where(&entities.Partners{ID: partner.ID}).Updates(&entities.Partners{LastSync: currentLastSync})

}

func Runner() {
	fmt.Println("Every second")
	var listPartner []entities.Partners
	database.DB.Model(&entities.Partners{}).Scan(&listPartner)
	fmt.Println(len(listPartner))
	for i := 0; i < len(listPartner); i++ {
		if listPartner[i].Type == "cukcuk" {
			HandlerCukCuk(listPartner[i])
		}
	}
}

func RunnerAfter2Day() {
	fmt.Println("Every second")
	var listPartner []entities.Partners
	database.DB.Model(&entities.Partners{}).Scan(&listPartner)
	fmt.Println(len(listPartner))
	for i := 0; i < len(listPartner); i++ {
		if listPartner[i].Type == "cukcuk" {

			listPartner[i].LastSync = listPartner[i].LastSync.AddDate(0, 0, -2)
			HandlerCukCuk(listPartner[i])
		}
	}
}

func StartJob() {
	//c := cron.New()
	//c.AddFunc("@every 0h5m30s", Runner)
	//c.AddFunc("@every 24h0m0s", RunnerAfter2Day)
	//c.Start()
	Runner()
}
