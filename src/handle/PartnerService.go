package handle

import (
	"encoding/json"
	"fmt"
	"os"
	"partner-sync/src/controllers"
	"partner-sync/src/models"
	"runtime"
	"shopf1/go-flow/kafka"
	"sync"
)

func InitConsumer() {
	runtime.GOMAXPROCS(8)

	var wg sync.WaitGroup
	wg.Add(8)
	for i := 0; i < 8; i++ {
		go func() {
			defer wg.Done()

			process()
		}()
	}

	wg.Wait()
}

func process() {
	consumer := kafka.InitConsumer()
	er := consumer.Subscribe(os.Getenv("KAFKA_PARTNER_TOPIC"), nil)
	if er != nil {
		fmt.Println(er)
	}
	for {
		msg, err := consumer.ReadMessage(-1)
		if err == nil {

			fmt.Printf("Message on %s: %s\n", msg.TopicPartition, string(msg.Value))
			message := models.PartnerMessage{}
			err = json.Unmarshal(msg.Value, &message)
			if err != nil {
				fmt.Printf("error parse json to object: %v", err)
			} else {
				fmt.Printf("sendMessage: %v", msg)
			}
			switch message.Type {
			case "HOA_NAM":
				jsonData, _ := json.Marshal(message.Data)
				controllers.HoaNamDataProcess(jsonData)
				break
			case "KIOT_VIET":
				jsonData, _ := json.Marshal(message.Data)
				controllers.KiotVietDataProcess(jsonData)
				break
			}

		} else {
			fmt.Println(err)
		}
	}
}
