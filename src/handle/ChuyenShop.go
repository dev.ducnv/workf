package handle

import (
	"fmt"
	"shopf1/go-flow/database"
	"shopf1/go-flow/entities"
)

type Count struct {
	Count int "gorm='count'"
}

func ChuyenShop() {
	var shops []entities.Shops
	database.DB.Model(&entities.Shops{}).Table("shops").Where("id in(133828)").Scan(&shops)

	for i := 0; i < len(shops); i++ {
		var count = Count{}
		database.DB.Raw(fmt.Sprintf(`UPDATE shoppro.shop_customers
	-- 		join tag_usage on tag_usage.usage_id = shop_customers.id
			SET shop_customers.shop_id  = %d, points = 0
			where shop_customers.shop_id != %d and (
	-- 		tag_usage.tag_id in(Select id from tags where shop_id = %d and type = 'customer') or
			 shop_customers.last_care  in(Select id from customer_care where shop_id = %d)
			)`, shops[i].ID, shops[i].ID, shops[i].ID, shops[i].ID)).Scan(&count)
	}

}
