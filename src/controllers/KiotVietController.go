package controllers

import (
	"encoding/json"
	"fmt"
	"partner-sync/src/jobs"
	"partner-sync/src/models"
	"shopf1/go-flow/database"
	"shopf1/go-flow/entities"
	"strconv"
	"strings"
	"time"
)

func KiotVietDataProcess(in []byte) {
	fmt.Println(string(in))
	var dataNotification = models.HookKiotVietNotifications{}
	errRes := json.Unmarshal(in, &dataNotification)

	if errRes != nil || dataNotification.Id == "" {
		fmt.Println("Data chua duoc xu ly")
		database.DB.Model(&entities.PartnerData{}).Create(&entities.PartnerData{
			ShopId: 121212,
			Data:   string(in),
		})
	} else if strings.Contains(dataNotification.Notifications[0].Action, "customer.update") {

		fmt.Println("Xử lý thông tin khách hàng")

		var data = models.KiotVietCustomer{}
		jsonData, _ := json.Marshal(dataNotification.Notifications[0].Data[0])
		err := json.Unmarshal(jsonData, &data)
		if err != nil {
			fmt.Println(err)
			return
		}

		var kiotVietId = strings.ReplaceAll(dataNotification.Notifications[0].Action, "customer.update.", "")

		var partner = entities.Partners{}
		database.DB.Model(&entities.Partners{}).Where(&entities.Partners{AppId: kiotVietId}).First(&partner)

		if partner.ID != 0 {

			// Kiểm tra xem đã tồn tại khách hàng trên hệ thống
			var partnerCustomer = entities.PartnerCustomers{}
			database.DB.Model(&entities.PartnerCustomers{}).Where(&entities.PartnerCustomers{PartnerCustomerId: kiotVietId + "_" + data.Code}).First(&partnerCustomer)

			// Nếu chưa có khách hàng thì tạo thêm khách hàng
			if partnerCustomer.ID == 0 {

				var shopCustomer = entities.ShopCustomers{
					ShopID:     partner.ShopId,
					SocialType: "kiotviet",
					FullName:   data.Name,
					Phone:      data.ContactNumber,
					Address:    data.Address,
				}
				database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ShopID: partner.ShopId, Phone: data.ContactNumber}).First(&shopCustomer)

				// Mapping khách hàng và kiotviet

				if shopCustomer.ID == 0 {
					shopCustomer = entities.ShopCustomers{
						ShopID:     partner.ShopId,
						SocialType: "kiotviet",
						FullName:   data.Name,
						Phone:      data.ContactNumber,
						Address:    data.Address,
					}
					database.DB.Model(&entities.ShopCustomers{}).Create(&shopCustomer)
				}

				database.DB.Model(&entities.PartnerCustomers{}).Create(&entities.PartnerCustomers{PartnerType: "kiotviet", ShopId: partner.ShopId, ShopCustomerId: shopCustomer.ID, PartnerCustomerId: kiotVietId + "_" + data.Code})

				if partner.Tags != "" {
					var tags = strings.Split(partner.Tags, ",")
					for _, tag := range tags {
						tagId, _ := strconv.Atoi(tag)
						database.DB.Model(&entities.TagUsage{}).Create(&entities.TagUsage{TagId: tagId, Type: "customer", UsageId: shopCustomer.ID})
					}
				}
			}
		}

	} else if strings.Contains(dataNotification.Notifications[0].Action, "invoice.update") {

		fmt.Println("Xử lý đơn hàng")
		var kiotVietId = strings.Replace(dataNotification.Notifications[0].Action, "invoice.update.", "", -1)

		var data = models.KiotVietInvoice{}
		jsonData, _ := json.Marshal(dataNotification.Notifications[0].Data[0])
		err := json.Unmarshal(jsonData, &data)
		if err != nil {
			fmt.Println(err)
			return
		}

		var partner = entities.Partners{}
		database.DB.Model(&entities.Partners{}).Where(&entities.Partners{AppId: kiotVietId}).First(&partner)

		if partner.ID != 0 {

			// Kiểm tra xem đã tồn tại khách hàng trên hệ thống
			var partnerCustomer = entities.PartnerCustomers{}
			database.DB.Model(&entities.PartnerCustomers{}).Where(&entities.PartnerCustomers{PartnerCustomerId: kiotVietId + "_" + data.CustomerCode}).First(&partnerCustomer)

			// Nếu chưa có khách hàng thì tạo thêm khách hàng
			var shopCustomer = entities.ShopCustomers{}

			if partnerCustomer.ID == 0 {

				shopCustomer = entities.ShopCustomers{
					ShopID:     partner.ShopId,
					SocialType: "kiotviet",
					FullName:   data.CustomerName,
					Phone:      data.InvoiceDelivery.ContactNumber,
					Address:    data.InvoiceDelivery.Address,
					LastCare:   partner.LastCare,
				}
				database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ShopID: partner.ShopId, Phone: data.InvoiceDelivery.ContactNumber}).First(&shopCustomer)

				// Mapping khách hàng và kiotviet

				if shopCustomer.ID == 0 {
					shopCustomer = entities.ShopCustomers{
						ShopID:     partner.ShopId,
						SocialType: "kiotviet",
						FullName:   data.CustomerName,
						Phone:      data.InvoiceDelivery.ContactNumber,
						Address:    data.InvoiceDelivery.Address,
						LastCare:   partner.LastCare,
					}
					database.DB.Model(&entities.ShopCustomers{}).Create(&shopCustomer)

					if partner.Tags != "" {
						var tags = strings.Split(partner.Tags, ",")
						for _, tag := range tags {
							tagId, _ := strconv.Atoi(tag)
							database.DB.Model(&entities.TagUsage{}).Create(&entities.TagUsage{TagId: tagId, Type: "customer", UsageId: shopCustomer.ID})
						}
					}

					go func() {
						defer func() {
							if errTrigger := recover(); errTrigger != nil {
								fmt.Sprintf("panic occurred")
							}
						}()

						jobs.SendTriggerToOneByLastCare(partner.LastCare, partner.ShopId, shopCustomer.ID)
					}()
				}

				database.DB.Model(&entities.PartnerCustomers{}).Create(&entities.PartnerCustomers{PartnerType: "kiotviet", ShopId: partner.ShopId, ShopCustomerId: shopCustomer.ID, PartnerCustomerId: kiotVietId + "_" + data.CustomerCode})
			}
			if partner.LastCare != 0 {
				database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ID: shopCustomer.ID}).Update("last_care", partner.LastCare)

				go func() {
					defer func() {
						if errTrigger := recover(); errTrigger != nil {
							fmt.Sprintf("panic occurred")
						}
					}()

					jobs.SendTriggerToOneByLastCare(partner.LastCare, partner.ShopId, shopCustomer.ID)
				}()
			}

			// Kiểm tra đơn hàng đã có trên hệ thống chưa
			var orderPartner = entities.OrderPartner{}
			database.DB.Model(&entities.OrderPartner{}).Where(&entities.OrderPartner{OrderPartnerId: kiotVietId + "_" + data.Code}).First(&orderPartner)

			// Nếu chưa tồn tại thì thêm vào hệ thống

			if orderPartner.ID == 0 {
				var feeShip = 0
				var totalAmount float32 = 0

				var productNames []string
				for _, product := range data.InvoiceDetails {
					productNames = append(productNames, fmt.Sprintf("%.1f %s", product.Quantity, product.ProductName))
					totalAmount += product.Quantity * float32(product.Price-product.Discount)
				}
				feeShip = data.Total - int(totalAmount)

				var status = 1
				if data.Status == 2 {
					status = -1
				}

				var customerName = data.InvoiceDelivery.Receiver
				if customerName == "" {
					customerName = shopCustomer.FullName
				}

				var customerPhone = data.InvoiceDelivery.ContactNumber
				if customerPhone == "" {
					customerPhone = shopCustomer.Phone
				}

				var codeAmount = 0

				if data.InvoiceDelivery.UsingPriceCod {
					codeAmount = data.Total
				}

				var order = entities.Orders{
					ShopID:           partner.ShopId,
					OrderNumber:      fmt.Sprintf("%d_%s", partner.ShopId, data.Code),
					CustomerID:       shopCustomer.ShopID,
					ShippingName:     customerName,
					ShippingAddress:  data.InvoiceDelivery.Address,
					ShippingPhone:    customerPhone,
					Total:            data.Total,
					CodAmount:        codeAmount,
					ShipFee:          feeShip,
					EstimateShipDate: time.Now(),
					Detail:           strings.Join(productNames, ", "),
					Status:           status,
					CreatedAt:        data.PurchaseDate,
				}
				database.DB.Model(&entities.Orders{}).Create(&order)

				database.DB.Model(&entities.OrderPartner{}).Create(&entities.OrderPartner{
					OrderId:        order.ID,
					OrderPartnerId: kiotVietId + "_" + data.Code,
					StatusText:     data.StatusValue,
				})
			} else {
				var order = entities.Orders{}
				database.DB.Model(&entities.Orders{}).Where(&entities.Orders{ID: orderPartner.OrderId}).First(&order)
				if order.ID != 0 {
					var status = 1
					if data.Status == 2 {
						status = -1
					}

					database.DB.Model(&entities.Orders{}).Where(&entities.Orders{ID: order.ID}).Update("status", status)
				}
			}
		}
	} else {
		fmt.Println("Data chua duoc xu ly")
		database.DB.Model(&entities.PartnerData{}).Create(&entities.PartnerData{
			ShopId: 121212,
			Data:   string(in),
		})
	}

}
