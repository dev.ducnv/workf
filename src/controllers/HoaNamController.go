package controllers

import (
	"common/database"
	"encoding/json"
	"fmt"
	"partner-sync/src/models"
	"shopf1/go-flow/entities"
)

func HoaNamDataProcess(in []byte) {
	var data = models.HoaNamData{}
	errRes := json.Unmarshal(in, &data)
	const ShopId = 133001

	if errRes != nil {
		fmt.Println("Data chua duoc xu ly")
		database.DB.Model(&entities.PartnerData{}).Create(&entities.PartnerData{
			ShopId: ShopId,
			Data:   string(in),
		})
	} else if data.Id == 0 {

		if data.Phone != "" {
			fmt.Println("Xử lý thông tin khách hàng")

			// Tìm khách hàng với sđt bắn sang - ShopId Hoa Nam - 133001

			var customer = entities.ShopCustomers{}

			database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ShopID: ShopId, Phone: data.Phone}).Last(&customer)

			if customer.ID != 0 && data.Phone != "000" {
				database.DB.Model(&entities.ShopCustomers{}).Create(&entities.PartnerCustomers{
					ShopId:            ShopId,
					PartnerType:       "hoa_nam",
					PartnerCustomerId: string(rune(data.Id)),
					ShopCustomerId:    customer.ID,
				})

			} else {

				// begin a transaction

				var shopCustomer = entities.ShopCustomers{
					ShopID:     ShopId,
					FullName:   data.Name,
					Phone:      data.Phone,
					SocialType: "partner",
					Address:    data.Address,
				}

				database.DB.Model(&entities.ShopCustomers{}).Create(&shopCustomer)

				database.DB.Model(&entities.ShopCustomers{}).Create(&entities.PartnerCustomers{
					ShopId:            ShopId,
					PartnerType:       "hoa_nam",
					PartnerCustomerId: string(rune(data.Id)),
					ShopCustomerId:    shopCustomer.ID,
				})
			}

		} else {
			fmt.Println("Xử lý thông tin đơn hàng")
			database.DB.Model(&entities.PartnerData{}).Create(&entities.PartnerData{
				ShopId: ShopId,
				Data:   string(in),
			})
			//var orderPartner = entities.OrderPartner{}
			//
			//database.DB.Model(&entities.OrderPartner{}).Where(&entities.OrderPartner{OrderPartnerId: string(rune(data.Id))}).First(&orderPartner)

			//var order = entities.Orders{}

			// Kiểm tra xem đã có đơn hàng đối tác trên hệ thống chưa
			//if orderPartner.ID == 0 {
			//	database.DB.Model(&entities.OrderPartner{}).Where(&entities.OrderPartner{ID: orderPartner.ID}).Updates(&entities.OrderPartner{
			//		StatusText: data.StatusText,
			//	})
			//} else {
			//
			//}

			//database.DB.Model(&entities.PartnerData{}).Create(&entities.PartnerData{
			//	ShopId: ShopId,
			//	Data:   in,
			//})

		}
	} else {
		fmt.Println("Data chua duoc xu ly")
		database.DB.Model(&entities.PartnerData{}).Create(&entities.PartnerData{
			ShopId: ShopId,
			Data:   string(in),
		})
	}

}
