package controllers

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/lithammer/shortuuid/v4"
	"github.com/mailersend/mailersend-go"
	"github.com/xuri/excelize/v2"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"partner-sync/src/models"
	"partner-sync/src/request"
	"partner-sync/src/response"
	"shopf1/go-flow/constants"
	"shopf1/go-flow/database"
	"shopf1/go-flow/entities"
	"shopf1/go-flow/utils"
	"strconv"
	"strings"
	"time"
)

func LandingPage(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()
	var requestBody = request.LandingPageRequest{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	var shopId = utils.StringToInt(r.Header.Get(constants.HeaderShopIdField))

	if shopId == 0 {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "Missing params"
	} else {
		tx := database.DB.Begin()
		tx.SavePoint("first")
		var total = 0

		var event = entities.Event{}
		tx.Model(&entities.Event{}).Where(&entities.Event{Id: requestBody.EventId}).First(&event)

		var shopCustomer = entities.ShopCustomers{
			ShopID:     event.ShopId,
			FullName:   requestBody.Guardian.FullName,
			Phone:      requestBody.Guardian.Phone,
			Email:      requestBody.Guardian.Email,
			SocialType: "ticket",
		}

		tx.Model(&entities.ShopCustomers{}).Create(&shopCustomer)

		var transaction = entities.TicketTransaction{
			ShopId:         shopId,
			Total:          total,
			ShopCustomerId: shopCustomer.ID,
			EventId:        event.Id,
		}
		if event.SellTicketType == 1 {
			transaction.CheckInId = "tran|" + shortuuid.New()
		}

		tx.Model(&entities.TicketTransaction{}).Create(&transaction)

		for i := 0; i < len(requestBody.Tickets); i++ {
			var ticketType = entities.TicketType{}
			tx.Model(&entities.TicketType{}).Where(&entities.TicketType{Id: requestBody.Tickets[i].TicketTypeId}).First(&ticketType)

			var ticketPrice = entities.TicketPrice{}
			tx.Model(&entities.TicketPrice{}).Where(&entities.TicketPrice{EventId: requestBody.EventId}).Where("? BETWEEN start_time AND end_time", time.Now()).First(&ticketPrice)

			requestBody.Tickets[i].ShopId = shopId
			requestBody.Tickets[i].TicketId = shortuuid.New()
			requestBody.Tickets[i].EventId = requestBody.EventId

			tx.Model(&entities.Ticket{}).Create(&requestBody.Tickets[i])

			var transactionLine = entities.TicketTransactionLine{
				TicketId:      requestBody.Tickets[i].TicketId,
				TransactionId: transaction.Id,
				Total:         ticketPrice.Price,
			}
			tx.Model(&entities.TicketTransactionLine{}).Create(&transactionLine)

			tx.Model(&entities.ShopCustomers{}).Create(&entities.ShopCustomers{
				ShopID:   event.ShopId,
				FullName: requestBody.Tickets[i].FullName,
				Phone:    requestBody.Tickets[i].Phone,
				//Sex: requestBody.Gender?,
				Email:      requestBody.Tickets[i].Email,
				SocialType: "ticket",
			})

			total += ticketPrice.Price

		}

		tx.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{Id: transaction.Id}).Updates(&entities.TicketTransaction{Total: total})

		if err := tx.Commit().Error; err != nil {
			tx.RollbackTo("first")
			res = response.BadRequest("")
			json.NewEncoder(w).Encode(res)
			return
		}

		if total == 0 {
			for i := 0; i < len(requestBody.Tickets); i++ {
				SendEmail2(requestBody.Tickets[i], event)
			}
		} else {
			res.Data = utils.GetLinkVNPay(total, strconv.Itoa(transaction.Id), event.PaymentCallback)
		}

	}
	json.NewEncoder(w).Encode(res)
}

func LandingPageSingle(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()
	var requestBody = entities.Ticket{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	if requestBody.ShopId == 0 || (utils.GetMD5Hash(fmt.Sprintf("%d@%d", requestBody.ShopId, requestBody.EventId)) != requestBody.EventToken) {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "Missing params or token incorrect"
	} else {
		tx := database.DB.Begin()
		tx.SavePoint("first")
		var total = 0
		var transaction = entities.TicketTransaction{
			ShopId: requestBody.ShopId,
			Total:  total,
		}
		tx.Model(&entities.TicketTransaction{}).Create(&transaction)

		var event = entities.Event{}
		tx.Model(&entities.Event{}).Where(&entities.Event{Id: requestBody.EventId}).First(&event)

		var ticketType = entities.TicketType{}
		tx.Model(&entities.TicketType{}).Where(&entities.TicketType{Id: requestBody.TicketTypeId}).First(&ticketType)

		var ticketPrice = entities.TicketPrice{}
		tx.Model(&entities.TicketPrice{}).Where(&entities.TicketPrice{EventId: requestBody.EventId}).Where("? BETWEEN start_time AND end_time", time.Now()).First(&ticketPrice)

		requestBody.TicketId = shortuuid.New()

		tx.Model(&entities.Ticket{}).Create(&requestBody)

		var transactionLine = entities.TicketTransactionLine{
			TicketId:      requestBody.TicketId,
			TransactionId: transaction.Id,
			Total:         ticketPrice.Price,
		}
		tx.Model(&entities.TicketTransactionLine{}).Create(&transactionLine)

		var shopCustomer = entities.ShopCustomers{
			ShopID:   event.ShopId,
			FullName: requestBody.FullName,
			Phone:    requestBody.Phone,
			//Sex: requestBody.Gender?,
			Email:      requestBody.Email,
			Note:       requestBody.Note,
			SocialType: "ticket",
		}

		database.DB.Model(&entities.ShopCustomers{}).Create(&shopCustomer)

		total += ticketPrice.Price

		tx.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{Id: transaction.Id}).Updates(&entities.TicketTransaction{Total: total})

		tx.Model(&entities.Ticket{}).Where(&entities.Ticket{TicketId: requestBody.TicketId}).Updates(&entities.Ticket{ShopCustomerId: shopCustomer.ID})

		if err := tx.Commit().Error; err != nil {
			tx.RollbackTo("first")
			res = response.BadRequest("")
			json.NewEncoder(w).Encode(res)
			return
		}

		if total == 0 {
			SendEmail2(requestBody, event)
			res.Data = requestBody
		} else {
			res.Data = utils.GetLinkVNPay(total, strconv.Itoa(transaction.Id), event.PaymentCallback)
		}

	}
	json.NewEncoder(w).Encode(res)
}

func ConfirmVNPay(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()

	var requestBody = models.VNPayCallback{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	var vnp_HashSecret = "CNQUQSIPZXEACNDYVTKFYJQDNZPEVJVV"
	var hashData = "vnp_Amount=" + strconv.Itoa(requestBody.Amount) + "&vnp_BankCode=" + requestBody.BankCode + "&vnp_BankTranNo=" + strconv.Itoa(requestBody.BankTranNo) + "&vnp_CardType=" + requestBody.CardType + "&vnp_OrderInfo=" + strings.Replace(requestBody.OrderInfo, "+", " ", -1) + "&vnp_PayDate=" + requestBody.PayDate + "&vnp_ResponseCode=" + requestBody.ResponseCode + "&vnp_TmnCode=" + requestBody.TmnCode + "&vnp_TransactionNo=" + requestBody.TransactionNo + "&vnp_TxnRef=" + strconv.Itoa(requestBody.TxnRef)

	//var vnpTranId = requestBody.TransactionNo //Mã giao dịch tại VNPAY
	var vnpSecureHash = requestBody.SecureHash

	var secureHash = utils.GetMD5Hash(vnp_HashSecret + hashData)
	var shopServiceId = requestBody.TxnRef
	if shopServiceId == 0 {
		w.WriteHeader(http.StatusBadRequest)
		res = response.BadRequest("missing shopid in header")
		res.Message = "99"
	} else {
		if secureHash == vnpSecureHash {

			if requestBody.OrderInfo == "" {
				w.WriteHeader(http.StatusBadRequest)
				res = response.BadRequest("missing shopid in header")
				res.Message = "01"
				res.Data = "Order not found"
			} else {
				if requestBody.ResponseCode == "00" {
					var transaction = entities.TicketTransaction{}
					database.DB.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{Id: requestBody.TxnRef}).First(&transaction)

					database.DB.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{Id: transaction.Id}).Updates(&entities.TicketTransaction{
						Paid:          transaction.Total,
						PaymentMethod: "VNPay",
						Status:        1,
					})

					var shopCustomer = entities.ShopCustomers{}
					database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ID: transaction.ShopCustomerId}).First(&shopCustomer)

					var event = entities.Event{}
					database.DB.Model(&entities.Event{}).Where(&entities.Event{Id: transaction.EventId}).First(&event)

					var tickets []entities.Ticket
					database.DB.Model(&entities.Ticket{}).Joins("JOIN ticket_transaction_lines ON ticket_transaction_lines.ticket_id=tickets.ticket_id").
						Where("ticket_transaction_lines.transaction_id = ?", transaction.Id).Scan(&tickets)

					for i := 0; i < len(tickets); i++ {

						var ticketType = entities.TicketType{}
						database.DB.Model(&entities.TicketType{}).Where(&entities.TicketType{Id: tickets[i].TicketTypeId}).First(&ticketType)

						if ticketType.StartBib != 0 {
							var count []entities.Count
							database.DB.Model(&entities.Ticket{}).Select("COUNT(*) as count").Where(&entities.Ticket{EventId: tickets[i].EventId, TicketTypeId: tickets[i].TicketTypeId}).Where(&entities.Ticket{Status: 1}).Scan(&count)
							database.DB.Model(&entities.Ticket{}).Where(&entities.Ticket{TicketId: tickets[i].TicketId}).Updates(&entities.Ticket{Status: 1, Bib: fmt.Sprintf("%d", ticketType.StartBib+count[0].Count+1)})
						} else {

							database.DB.Model(&entities.Ticket{}).Where(&entities.Ticket{TicketId: tickets[i].TicketId}).Updates(&entities.Ticket{Status: 1})
						}
						if event.SellTicketType != 1 {
							SendEmail2(tickets[i], event)
						}
					}

					if event.SellTicketType == 1 {
						SendEmailTransaction(transaction, shopCustomer, event)
					}

					res.Message = "00"
					res.Data = event.Link
				} else {

					res.Message = "00"
					res.Data = "Confirm Success 1 - xac nhan huy don"
				}

			}
		} else {
			res.Message = "97"
			res.Data = "Chu ky khong hop le"
		}
	}

	json.NewEncoder(w).Encode(res)

}

func SendEmail(ticket entities.Ticket) {
	ms := mailersend.NewMailersend(os.Getenv("MAILERSEND_API_KEY"))

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	subject := "Subject"
	text := "This is the text content"
	html := fmt.Sprintf("<p>This is the HTML content</p><img src='https://quickchart.io/qr?text=%s&amp;size=300'", ticket.TicketId)

	from := mailersend.From{
		Name:  "Biglead",
		Email: "admin@wisell.com.mm",
	}

	recipients := []mailersend.Recipient{
		{
			Name:  ticket.FullName,
			Email: ticket.Email,
		},
	}

	// Send in now
	sendAt := time.Now().Unix()

	//tags := []string{"foo", "bar"}

	message := ms.Email.NewMessage()

	message.SetFrom(from)
	message.SetRecipients(recipients)
	message.SetSubject(subject)
	message.SetHTML(html)
	message.SetText(text)
	message.SetSendAt(sendAt)
	message.SetInReplyTo("client-id")

	res, _ := ms.Email.Send(ctx, message)

	fmt.Printf(res.Header.Get("X-Message-Id"))
}

type Payload struct {
	From     string `json:"from"`
	To       string `json:"to"`
	Subject  string `json:"subject"`
	Htmlbody string `json:"htmlbody"`
}
type From struct {
	Address string `json:"address"`
}
type EmailAddress struct {
	Address string `json:"address"`
	Name    string `json:"name"`
}
type To struct {
	EmailAddress EmailAddress `json:"email_address"`
}

func SendEmail2(ticket entities.Ticket, event entities.Event) {

	var ticketTemplate = entities.TicketTemplate{}
	database.DB.Model(&entities.TicketTemplate{}).Joins("join ticket_types on ticket_types.template_id=ticket_templates.id").Where("ticket_types.id = ?", ticket.TicketTypeId).Select("ticket_templates.*").First(&ticketTemplate)

	contentDecode, err := base64.StdEncoding.DecodeString(ticketTemplate.Content)
	if err != nil {
		panic(err)
	}

	var content = fmt.Sprintf("%s", contentDecode)

	content = strings.ReplaceAll(content, "{{email}}", ticket.Email)
	content = strings.ReplaceAll(content, "{{bib}}", ticket.Bib)
	content = strings.ReplaceAll(content, "{{phone}}", ticket.Phone)
	content = strings.ReplaceAll(content, "{{full_name}}", ticket.FullName)
	content = strings.ReplaceAll(content, "{{qrcode}}", ticket.TicketId)

	//data := Payload{
	//	From:     "noreply@shoptool.vn",
	//	To:       ticket.Email,
	//	Subject:  "Vé online sự kiện " + event.Name,
	//	Htmlbody: content,
	//}

	//fileDir, _ := os.Getwd()
	//fileName := "upload-file.txt"
	//filePath := path.Join(fileDir, fileName)

	//file, _ := os.Open(filePath)
	//defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	writer.WriteField("from", ticketTemplate.EmailName+" <noreply@shoptool.vn>")
	writer.WriteField("to", ticket.Email)
	writer.WriteField("to", "thaotp.biglead@gmail.com")
	writer.WriteField("subject", "Vé online sự kiện "+event.Name)
	writer.WriteField("html", content)
	//part, _ := writer.CreateFormFile("attachment", filepath.Base(file.Name()))
	//io.Copy(part, file)
	writer.Close()

	//payloadBytes, err := json.Marshal(data)
	//if err != nil {
	//	// handle err
	//}
	//body := bytes.NewReader(payloadBytes)

	req, err := http.NewRequest("POST", "https://api.mailgun.net/v3/shoptool.vn/messages", body)
	if err != nil {
		// handle err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Basic YXBpOjBkN2ExZjZjZmJlZjE0MTExMTRjYzAyMzI1MjhmNGFkLTVkOWJkODNjLTg0NWY4MGYy")
	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
	}
	defer resp.Body.Close()
}

func SendEmailTransaction(transaction entities.TicketTransaction, shopCustomer entities.ShopCustomers, event entities.Event) {

	var ticketTemplate = entities.TicketTemplate{}
	database.DB.Model(&entities.TicketTemplate{}).Where(&entities.TicketTemplate{Id: event.TemplateId}).First(&ticketTemplate)

	contentDecode, err := base64.StdEncoding.DecodeString(ticketTemplate.Content)
	if err != nil {
		panic(err)
	}

	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	excel, err := excelize.OpenFile(pwd + "/assets/Danh_sach_dang_ky_PMG.xlsx")

	const (
		STT               string = "A"
		NGM_HoTen         string = "B"
		NGM_SDT           string = "C"
		DaThanhToan       string = "D"
		STT_VDV           string = "E"
		HoTen             string = "F"
		GioiTinh          string = "G"
		NamSinh           string = "H"
		TruongHoc         string = "I"
		ThanhTich         string = "J"
		DangKyThiTichDiem string = "K"
		Loai_67           string = "L"
		Loai_89           string = "M"
		Loai_1012         string = "N"
		Ten_BIB           string = "O"
		BIB               string = "P"
		So_3              string = "Q"
		So_4              string = "R"
		So_5              string = "S"
		So_6              string = "T"
		XS                string = "U"
		S                 string = "V"
		M                 string = "W"
		L                 string = "X"
		NhomMau           string = "Y"
		TienSuBenh        string = "Z"
	)

	if err != nil {
		fmt.Println(err)
	}
	sheetName := "DS VĐV "
	if err != nil {
		fmt.Println(err)
	}

	var customer = entities.ShopCustomers{}
	database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ID: transaction.ShopCustomerId}).First(&customer)

	var tickets []entities.Ticket
	database.DB.Model(&entities.Ticket{}).Joins("join ticket_transaction_lines on ticket_transaction_lines.ticket_id = tickets.ticket_id").Where("ticket_transaction_lines.transaction_id = ?", transaction.Id).Scan(&tickets)

	var rowPos = 4

	excel.SetCellInt(sheetName, STT+fmt.Sprintf("%d", rowPos), 1)
	excel.SetCellStr(sheetName, NGM_HoTen+fmt.Sprintf("%d", rowPos), customer.FullName)
	excel.SetCellStr(sheetName, NGM_SDT+fmt.Sprintf("%d", rowPos), customer.Phone)
	excel.SetCellStr(sheetName, DaThanhToan+fmt.Sprintf("%d", rowPos), "x")
	rowPos += 1

	for i := 0; i < len(tickets); i++ {
		excel.SetCellInt(sheetName, STT_VDV+fmt.Sprintf("%d", rowPos), i+1)
		excel.SetCellStr(sheetName, HoTen+fmt.Sprintf("%d", rowPos), tickets[i].FullName)
		excel.SetCellStr(sheetName, GioiTinh+fmt.Sprintf("%d", rowPos), utils.IfThenElse(tickets[i].Gender == 1, "Nữ", "Nam"))
		excel.SetCellStr(sheetName, NamSinh+fmt.Sprintf("%d", rowPos), tickets[i].Birthday.Format("2006"))
		excel.SetCellStr(sheetName, TruongHoc+fmt.Sprintf("%d", rowPos), tickets[i].Organization)
		excel.SetCellStr(sheetName, ThanhTich+fmt.Sprintf("%d", rowPos), "Có")
		if tickets[i].TicketTypeId == 17 {
			excel.SetCellStr(sheetName, Loai_67+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].TicketTypeId == 18 {
			excel.SetCellStr(sheetName, Loai_89+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].TicketTypeId == 19 {
			excel.SetCellStr(sheetName, Loai_1012+fmt.Sprintf("%d", rowPos), "x")
		}
		excel.SetCellStr(sheetName, Ten_BIB+fmt.Sprintf("%d", rowPos), tickets[i].AliasName)

		if tickets[i].Size == "Số 3" {
			excel.SetCellStr(sheetName, So_3+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "Số 4" {
			excel.SetCellStr(sheetName, So_4+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "Số 5" {
			excel.SetCellStr(sheetName, So_5+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "Số 6" {
			excel.SetCellStr(sheetName, So_6+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "XS" {
			excel.SetCellStr(sheetName, XS+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "S" {
			excel.SetCellStr(sheetName, S+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "M" {
			excel.SetCellStr(sheetName, S+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "L" {
			excel.SetCellStr(sheetName, L+fmt.Sprintf("%d", rowPos), "x")
		}
		excel.SetCellStr(sheetName, NhomMau+fmt.Sprintf("%d", rowPos), tickets[i].BloodGroup)
		excel.SetCellStr(sheetName, TienSuBenh+fmt.Sprintf("%d", rowPos), tickets[i].MedicalHistory)
		rowPos += 1
	}

	filePath := pwd + "/assets/Danh_sach_dang_ky_PMG_" + utils.IntToString(int(time.Now().Unix())) + ".xlsx"
	excel.SaveAs(filePath)

	var content = fmt.Sprintf("%s", contentDecode)

	content = strings.ReplaceAll(content, "{{email}}", shopCustomer.Email)
	content = strings.ReplaceAll(content, "{{full_name}}", shopCustomer.FullName)
	content = strings.ReplaceAll(content, "{{qrcode}}", transaction.CheckInId)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	writer.WriteField("from", "Biglead <ducnv@shoptool.vn>")
	writer.WriteField("to", shopCustomer.Email)
	writer.WriteField("subject", "Vé online sự kiện "+event.Name)
	writer.WriteField("html", content)
	part, _ := writer.CreateFormFile("attachment", filePath)

	file, err := os.Open(filePath)
	if err != nil {
		// handle error
	}
	defer file.Close()

	io.Copy(part, file)
	writer.Close()

	//payloadBytes, err := json.Marshal(data)
	//if err != nil {
	//	// handle err
	//}
	//body := bytes.NewReader(payloadBytes)

	req, err := http.NewRequest("POST", "https://api.mailgun.net/v3/shoptool.vn/messages", body)
	if err != nil {
		// handle err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Basic YXBpOjBkN2ExZjZjZmJlZjE0MTExMTRjYzAyMzI1MjhmNGFkLTVkOWJkODNjLTg0NWY4MGYy")
	req.Header.Set("Content-Type", writer.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	bodyRes, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(bodyRes))
	if err != nil {
		// handle err
	}
	defer resp.Body.Close()
}

func SendEmailTicket(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()

	var requestBody = models.SendEmailTicket{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	if strings.Contains(requestBody.TicketId, "tran|") {
		var transaction = entities.TicketTransaction{}
		database.DB.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{CheckInId: requestBody.TicketId}).First(&transaction)
		if transaction.Id != 0 {
			var customer = entities.ShopCustomers{}
			database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ID: transaction.ShopCustomerId}).First(&customer)

			var event = entities.Event{}
			database.DB.Model(&entities.Event{}).Where(&entities.Event{Id: transaction.EventId}).First(&event)

			go SendEmailTransaction(transaction, customer, event)
		}
	} else {
		var ticket = entities.Ticket{}
		database.DB.Model(&entities.Ticket{}).Where(&entities.Ticket{TicketId: requestBody.TicketId}).First(&ticket)
		if ticket.EventId != 0 {
			var event = entities.Event{}
			database.DB.Model(&entities.Event{}).Where(&entities.Event{Id: ticket.EventId}).First(&event)

			if event.Id != 0 {
				go SendEmail2(ticket, event)
			}
		}
	}

	json.NewDecoder(r.Body).Decode(&res)

}

func PaymentTransaction(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()

	var requestBody = models.PaymentTicket{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	var transaction = entities.TicketTransaction{}
	database.DB.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{Id: requestBody.TransactionId}).First(&transaction)
	if transaction.Id != 0 {

		var event = entities.Event{}
		database.DB.Model(&entities.Event{}).Where(&entities.Event{Id: transaction.EventId}).First(&event)

		if event.Id != 0 {
			if requestBody.PaymentMethod == "VNPay" {
				res.Data = utils.GetLinkVNPay(transaction.Total, strconv.Itoa(transaction.Id), event.PaymentCallback)
			} else {
				database.DB.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{Id: requestBody.TransactionId}).Updates(&entities.TicketTransaction{
					Paid:          transaction.Total,
					PaymentMethod: requestBody.PaymentMethod,
					Status:        1,
				})

				var tickets []entities.Ticket
				database.DB.Model(&entities.Ticket{}).Joins("JOIN ticket_transaction_lines ON ticket_transaction_lines.ticket_id=tickets.ticket_id").
					Where("ticket_transaction_lines.transaction_id = ?", transaction.Id).Scan(&tickets)

				for i := 0; i < len(tickets); i++ {

					var ticketType = entities.TicketType{}
					database.DB.Model(&entities.TicketType{}).Where(&entities.TicketType{Id: tickets[i].TicketTypeId}).First(&ticketType)

					if ticketType.StartBib != 0 {
						var count []entities.Count
						database.DB.Model(&entities.Ticket{}).Select("COUNT(*) as count").Where(&entities.Ticket{EventId: tickets[i].EventId, TicketTypeId: tickets[i].TicketTypeId}).Where(&entities.Ticket{Status: 1}).Scan(&count)
						database.DB.Model(&entities.Ticket{}).Where(&entities.Ticket{TicketId: tickets[i].TicketId}).Updates(&entities.Ticket{Status: 1, Bib: fmt.Sprintf("%d", ticketType.StartBib+count[0].Count+1)})
					} else {

						database.DB.Model(&entities.Ticket{}).Where(&entities.Ticket{TicketId: tickets[i].TicketId}).Updates(&entities.Ticket{Status: 1})
					}

					SendEmail2(tickets[i], event)
				}
			}

		}

	}

	json.NewDecoder(r.Body).Decode(&res)

}

func Test(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()

	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	excel, err := excelize.OpenFile(pwd + "/assets/Danh_sach_dang_ky_PMG.xlsx")

	const (
		STT               string = "A"
		NGM_HoTen         string = "B"
		NGM_SDT           string = "C"
		DaThanhToan       string = "D"
		STT_VDV           string = "E"
		HoTen             string = "F"
		GioiTinh          string = "G"
		NamSinh           string = "H"
		TruongHoc         string = "I"
		ThanhTich         string = "J"
		DangKyThiTichDiem string = "K"
		Loai_67           string = "L"
		Loai_89           string = "M"
		Loai_1012         string = "N"
		Ten_BIB           string = "O"
		BIB               string = "P"
		So_3              string = "Q"
		So_4              string = "R"
		So_5              string = "S"
		So_6              string = "T"
		XS                string = "U"
		S                 string = "V"
		M                 string = "W"
		L                 string = "X"
		NhomMau           string = "Y"
		TienSuBenh        string = "Z"
	)

	if err != nil {
		fmt.Println(err)
	}
	sheetName := "DS VĐV "
	if err != nil {
		fmt.Println(err)
	}

	var transaction = entities.TicketTransaction{}
	database.DB.Model(&entities.TicketTransaction{}).Where(&entities.TicketTransaction{CheckInId: "tran|AFskNdyBPzHkR48LxCSxij"}).First(&transaction)

	var customer = entities.ShopCustomers{}
	database.DB.Model(&entities.ShopCustomers{}).Where(&entities.ShopCustomers{ID: transaction.ShopCustomerId}).First(&customer)

	var tickets []entities.Ticket
	database.DB.Model(&entities.Ticket{}).Joins("join ticket_transaction_lines on ticket_transaction_lines.ticket_id = tickets.ticket_id").Where("ticket_transaction_lines.transaction_id = ?", transaction.Id).Scan(&tickets)

	var rowPos = 4

	excel.SetCellInt(sheetName, STT+fmt.Sprintf("%d", rowPos), 1)
	excel.SetCellStr(sheetName, NGM_HoTen+fmt.Sprintf("%d", rowPos), customer.FullName)
	excel.SetCellStr(sheetName, NGM_SDT+fmt.Sprintf("%d", rowPos), customer.Phone)
	excel.SetCellStr(sheetName, DaThanhToan+fmt.Sprintf("%d", rowPos), "x")
	rowPos += 1

	for i := 0; i < len(tickets); i++ {
		excel.SetCellInt(sheetName, STT_VDV+fmt.Sprintf("%d", rowPos), i+1)
		excel.SetCellStr(sheetName, HoTen+fmt.Sprintf("%d", rowPos), tickets[i].FullName)
		excel.SetCellStr(sheetName, GioiTinh+fmt.Sprintf("%d", rowPos), utils.IfThenElse(tickets[i].Gender == 1, "Nữ", "Nam"))
		excel.SetCellStr(sheetName, NamSinh+fmt.Sprintf("%d", rowPos), tickets[i].Birthday.Format("2006"))
		excel.SetCellStr(sheetName, TruongHoc+fmt.Sprintf("%d", rowPos), tickets[i].Organization)
		excel.SetCellStr(sheetName, ThanhTich+fmt.Sprintf("%d", rowPos), "Có")
		if tickets[i].TicketTypeId == 17 {
			excel.SetCellStr(sheetName, Loai_67+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].TicketTypeId == 18 {
			excel.SetCellStr(sheetName, Loai_89+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].TicketTypeId == 19 {
			excel.SetCellStr(sheetName, Loai_1012+fmt.Sprintf("%d", rowPos), "x")
		}
		excel.SetCellStr(sheetName, Ten_BIB+fmt.Sprintf("%d", rowPos), tickets[i].AliasName)

		if tickets[i].Size == "Số 3" {
			excel.SetCellStr(sheetName, So_3+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "Số 4" {
			excel.SetCellStr(sheetName, So_4+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "Số 5" {
			excel.SetCellStr(sheetName, So_5+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "Số 6" {
			excel.SetCellStr(sheetName, So_6+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "XS" {
			excel.SetCellStr(sheetName, XS+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "S" {
			excel.SetCellStr(sheetName, S+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "M" {
			excel.SetCellStr(sheetName, S+fmt.Sprintf("%d", rowPos), "x")
		}
		if tickets[i].Size == "L" {
			excel.SetCellStr(sheetName, L+fmt.Sprintf("%d", rowPos), "x")
		}
		excel.SetCellStr(sheetName, NhomMau+fmt.Sprintf("%d", rowPos), tickets[i].BloodGroup)
		excel.SetCellStr(sheetName, TienSuBenh+fmt.Sprintf("%d", rowPos), tickets[i].MedicalHistory)
		rowPos += 1
	}

	var path = "/assets/Danh_sach_dang_ky_PMG_" + utils.IntToString(int(time.Now().Unix())) + ".xlsx"
	filePath := pwd + path
	excel.SaveAs(filePath)

	res.Data = path

	json.NewDecoder(r.Body).Decode(&res)

}
