package controllers

import (
	"encoding/json"
	"github.com/gorilla/schema"
	"net/http"
	"partner-sync/src/request"
	"partner-sync/src/response"
)

func GetPartners(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()
	var queryParams = request.QueryListRequest{}
	schema.NewDecoder().Decode(&queryParams, r.URL.Query())

	validation := request.QueryListValidation(queryParams)
	if validation != "" {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "Missing params"
	} else {
		var partners []entities.Partners

		database.DB.Model(&entities.Partners{}).Where(&entities.Partners{ShopId: queryParams.ShopId}).Scan(&partners)

		res.Data = partners
	}
	json.NewEncoder(w).Encode(res)
}

func CreatePartner(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()
	var requestBody = entities.Partners{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	validation := request.CreatePartnerValidation(requestBody)
	if validation != "" {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "Missing params"
	} else {
		database.DB.Model(&entities.Partners{}).Where(&entities.Partners{ShopId: requestBody.ShopId}).Create(&requestBody)
		res.Data = requestBody
	}
	json.NewEncoder(w).Encode(res)
}

func UpdatePartner(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()
	var requestBody = entities.Partners{}

	json.NewDecoder(r.Body).Decode(&requestBody)

	validation := request.UpdatePartnerValidation(requestBody)
	if validation != "" {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "Missing params"
	} else {
		database.DB.Model(&entities.Partners{}).Where(&entities.Partners{ShopId: requestBody.ShopId, ID: requestBody.ID}).Updates(&requestBody)
		res.Data = requestBody
	}
	json.NewEncoder(w).Encode(res)
}

func DeletePartner(w http.ResponseWriter, r *http.Request) {
	var res = response.DefaultResponse()
	var queryParams = entities.Partners{}
	schema.NewDecoder().Decode(&queryParams, r.URL.Query())

	validation := request.DeletePartnerValidation(queryParams)
	if validation != "" {
		w.WriteHeader(http.StatusBadRequest)
		res.Message = "Missing params"
	} else {
		database.DB.Model(&entities.Partners{}).Where(&entities.Partners{ShopId: queryParams.ShopId, ID: queryParams.ID}).Delete(&queryParams)
		res.Data = queryParams
	}
	json.NewEncoder(w).Encode(res)
}
