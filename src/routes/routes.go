package routes

import (
	"github.com/gorilla/mux"
	"net/http"
	"partner-sync/src/controllers"
)

const Partner string = "/partner"
const LandingPage string = "/landing-page"
const LandingPageSingle string = "/landing-page/single"
const ConfirmVNPay string = "/confirm_vnpay"
const Tickets string = "/tickets"
const Payments string = "/payments"
const Test string = "/test"

func Handle(router *mux.Router) {
	router.HandleFunc(Partner, controllers.GetPartners).Methods(http.MethodGet, http.MethodOptions)
	router.HandleFunc(Partner, controllers.CreatePartner).Methods(http.MethodPost, http.MethodOptions)
	router.HandleFunc(Partner, controllers.UpdatePartner).Methods(http.MethodPut, http.MethodOptions)
	router.HandleFunc(Partner, controllers.DeletePartner).Methods(http.MethodDelete, http.MethodOptions)

	router.HandleFunc(LandingPage, controllers.LandingPage).Methods(http.MethodPost, http.MethodOptions)
	router.HandleFunc(LandingPageSingle, controllers.LandingPageSingle).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(ConfirmVNPay, controllers.ConfirmVNPay).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(Tickets, controllers.SendEmailTicket).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(Payments, controllers.PaymentTransaction).Methods(http.MethodPost, http.MethodOptions)

	router.HandleFunc(Test, controllers.Test).Methods(http.MethodGet, http.MethodOptions)

}
