package middlewares

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/getsentry/sentry-go"
	"github.com/thoas/go-funk"
	"io/ioutil"
	"net/http"
	response2 "partner-sync/src/response"
	"partner-sync/src/routes"
	"runtime/debug"
	"shopf1/go-flow/constants"
	"shopf1/go-flow/externalapi"
	"shopf1/go-flow/externalapi/request"
	"strconv"
	"strings"
	"time"
)

var excludes = []string{
	routes.LandingPage,
	routes.ConfirmVNPay,
	routes.Tickets,
	routes.LandingPageSingle,
}

func Handle(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL.Path)
		if !funk.ContainsString(excludes, r.URL.Path) {
			w.Header().Set("Content-Type", "application/json")
			tokenString := r.Header.Get("Authorization")
			if tokenString != "" {
				tokenString = strings.Replace(tokenString, "Bearer ", "", 1)
				r.Header.Set(constants.HeaderTokenField, tokenString)
				parse, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
					return []byte("s"), nil
				})
				if err != nil {
					fmt.Println(err)
					//ErrorHeader(w,"header parser error")
					//return
				}
				if parse != nil {
					claims := parse.Claims.(jwt.MapClaims)
					if claims["name"] != nil {
						name := claims["name"].(string)
						r.Header.Set("_name", name)
					}
					if claims["id"] != nil {
						id := claims["id"].(float64)
						r.Header.Set("_id", strconv.FormatFloat(id, 'f', -1, 64))
					} else {
						ErrorHeader(w, "header parser is not found id")
						return
					}
				} else {
					ErrorHeader(w, "header is empty")
					return
				}
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(response2.Unauthorized(""))
				return
			}
		}
		requestCapturing := request.CapturingRequest{}
		requestCapturing.Headers = r.Header
		var body interface{}
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Println(err)
		}
		json.Unmarshal(data, &body)
		requestCapturing.Body = body
		requestCapturing.URI = r.URL.RequestURI()
		go externalapi.SendToLogServer(requestCapturing)
		r.Body = ioutil.NopCloser(bytes.NewReader(data))
		h.ServeHTTP(w, r)
	})
}

func NotFoundHandle(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	json.NewEncoder(w).Encode(response2.NotFound(""))
}

func MethodNotAllowed(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	json.NewEncoder(w).Encode(response2.MethodNotAllowed(""))
}
func ThrowCatch(w http.ResponseWriter) {
	err := recover()
	if err != nil {
		err = fmt.Errorf("%v, %s", err, string(debug.Stack()))
		sentry.CurrentHub().Recover(err)
		sentry.Flush(time.Second * 5)
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(response2.InternalServerError(""))
	}
}

func ErrorHeader(w http.ResponseWriter, message string) {
	w.WriteHeader(http.StatusBadRequest)
	json.NewEncoder(w).Encode(response2.BadRequest(message))
}
