package response

import "partner-sync/src/models"

type CukCukResponse struct {
	Code    int              `json:"Code" schema:"Code"`
	Success bool             `json:"Success" schema:"Success"`
	Data    models.DataLogin `json:"Data" schema:"Data"`
}

type CukCukCustomerResponse struct {
	Code    int                     `json:"Code" schema:"Code"`
	Total   int                     `json:"Total" schema:"Total"`
	Success bool                    `json:"Success" schema:"Success"`
	Data    []models.CukCukCustomer `json:"Data" schema:"Data"`
}

type CukCukBranchResponse struct {
	Code    int                   `json:"Code" schema:"Code"`
	Total   int                   `json:"Total" schema:"Total"`
	Success bool                  `json:"Success" schema:"Success"`
	Data    []models.CukCukBranch `json:"Data" schema:"Data"`
}

type CukCukOrderResponse struct {
	Code    int                  `json:"Code" schema:"Code"`
	Total   int                  `json:"Total" schema:"Total"`
	Success bool                 `json:"Success" schema:"Success"`
	Data    []models.CukCukOrder `json:"Data" schema:"Data"`
}
