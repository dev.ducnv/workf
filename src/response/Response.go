package response

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"net/http"
	"sort"
)

const SUCCESS string = "Success"
const ErrorInputTypeIncorrect = "Type incorrect!"
const ShopIdRequired = "shopid is required"

type Response struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
type MetaData struct {
	Page        int `json:"page"`
	PageSize    int `json:"page_size"`
	RecordTotal int `json:"record_total"`
}

type MetaDataResponse struct {
	Response
	MetaData MetaData `json:"meta_data"`
}

func DefaultResponse() Response {
	return Response{200, SUCCESS, nil}
}

func DefaultMetaDataResponse() MetaDataResponse {
	return MetaDataResponse{Response: Response{200, SUCCESS, nil}}
}

func (e Response) Error() string {
	return e.Message
}

func (e Response) StatusCode() int {
	return e.Status
}

func InternalServerError(msg string) Response {
	if msg == "" {
		msg = "We encountered an error while processing your models."
	}
	return Response{
		Status:  http.StatusInternalServerError,
		Message: msg,
	}
}

func NotFound(msg string) Response {
	if msg == "" {
		msg = "The requested resource was not found."
	}
	return Response{
		Status:  http.StatusNotFound,
		Message: msg,
	}
}

func Unauthorized(msg string) Response {
	if msg == "" {
		msg = "You are not authenticated to perform the requested action."
	}
	return Response{
		Status:  http.StatusUnauthorized,
		Message: msg,
	}
}
func Forbidden(msg string) Response {
	if msg == "" {
		msg = "You are not authorized to perform the requested action."
	}
	return Response{
		Status:  http.StatusForbidden,
		Message: msg,
	}
}
func BadRequest(msg string) Response {
	if msg == "" {
		msg = "Your models is in a bad format."
	}
	return Response{
		Status:  http.StatusBadRequest,
		Message: msg,
	}
}
func MetadataBadRequest(msg string) MetaDataResponse {
	if msg == "" {
		msg = "Your models is in a bad format."
	}
	return MetaDataResponse{Response: Response{http.StatusBadRequest, msg, nil}}
}
func MethodNotAllowed(msg string) Response {
	if msg == "" {
		msg = "Method Not Allowed"
	}
	return Response{
		Status:  http.StatusMethodNotAllowed,
		Message: msg,
	}
}

type invalidField struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// InvalidInput creates a new error response representing a data validation error (HTTP 400).
func InvalidInput(errs validation.Errors) Response {
	var details []invalidField
	var fields []string
	for field := range errs {
		fields = append(fields, field)
	}
	sort.Strings(fields)
	for _, field := range fields {
		details = append(details, invalidField{
			Field: field,
			Error: errs[field].Error(),
		})
	}

	return Response{
		Status:  http.StatusBadRequest,
		Message: "There is some problem with the data you submitted.",
	}
}
