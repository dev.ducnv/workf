package models

type LandingPage struct {
	ActionType string `json:"ActionType" schema:"ActionType"`
	ActionId   string `json:"ActionId" schema:"ActionId"`
}

type VNPayCallback struct {
	Amount         int    `json:"vnp_Amount" schema:"vnp_Amount"`
	BankCode       string `json:"vnp_BankCode" schema:"vnp_BankCode"`
	BankTranNo     int    `json:"vnp_BankTranNo" schema:"vnp_BankTranNo"`
	CardType       string `json:"vnp_CardType" schema:"vnp_CardType"`
	OrderInfo      string `json:"vnp_OrderInfo" schema:"vnp_OrderInfo"`
	PayDate        string `json:"vnp_PayDate" schema:"vnp_PayDate"`
	ResponseCode   string `json:"vnp_ResponseCode" schema:"vnp_ResponseCode"`
	TmnCode        string `json:"vnp_TmnCode" schema:"vnp_TmnCode"`
	TransactionNo  string `json:"vnp_TransactionNo" schema:"vnp_TransactionNo"`
	TxnRef         int    `json:"vnp_TxnRef" schema:"vnp_TxnRef"`
	SecureHashType string `json:"vnp_SecureHashType" schema:"vnp_SecureHashType"`
	SecureHash     string `json:"vnp_SecureHash" schema:"vnp_SecureHash"`
}

type SendEmailTicket struct {
	TicketId string `json:"ticket_id" schema:"ticket_id"`
}

type PaymentTicket struct {
	TransactionId int    `json:"transaction_id" schema:"transaction_id"`
	PaymentMethod string `json:"payment_method" schema:"payment_method"`
}
