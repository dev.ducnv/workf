package models

import "time"

type DataLogin struct {
	Domain      string `json:"Domain" schema:"Domain"`
	AppID       string `json:"AppID" schema:"AppID"`
	AccessToken string `json:"AccessToken" schema:"AccessToken"`
	CompanyCode string `json:"CompanyCode" schema:"CompanyCode"`
}

type CukCukCustomer struct {
	Id            string    `json:"Id" schema:"Id"`
	Code          string    `json:"Code" schema:"Code"`
	Name          string    `json:"Name" schema:"Name"`
	Tel           string    `json:"Tel" schema:"Tel"`
	Birthday      time.Time `json:"Birthday" schema:"Birthday"`
	Address       string    `json:"Address" schema:"Address"`
	Email         string    `json:"Email" schema:"Email"`
	Inactive      bool      `json:"Inactive" schema:"Inactive"`
	NormalizedTel string    `json:"NormalizedTel" schema:"NormalizedTel"`
	TotalAmount   float32   `json:"TotalAmount" schema:"TotalAmount"`
}

type CukCukBranch struct {
	Id      string `json:"Id" schema:"Id"`
	Code    string `json:"Code" schema:"Code"`
	Name    string `json:"Name" schema:"Name"`
	Address string `json:"Address" schema:"Address"`
	Tel     string `json:"Tel" schema:"Tel"`
}

type CukCukOrder struct {
	Id             string    `json:"Id" schema:"Id"`
	Type           int       `json:"Type" schema:"Type"`
	No             string    `json:"No" schema:"No"`
	Address        string    `json:"Address" schema:"Address"`
	BranchId       string    `json:"BranchId" schema:"BranchId"`
	BranchName     string    `json:"BranchName" schema:"BranchName"`
	CustomerId     string    `json:"CustomerId" schema:"CustomerId"`
	CustomerName   string    `json:"CustomerName" schema:"CustomerName"`
	CustomerTel    string    `json:"CustomerTel" schema:"CustomerTel"`
	Status         int       `json:"Status" schema:"Status"`
	Date           time.Time `json:"Date" schema:"Date"`
	TotalAmount    float32   `json:"TotalAmount" schema:"TotalAmount"`
	DeliveryAmount float32   `json:"DeliveryAmount" schema:"DeliveryAmount"`
	DepositAmount  float32   `json:"DepositAmount" schema:"DepositAmount"`
}
