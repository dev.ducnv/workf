package models

import "time"

type HookKiotViet struct {
	Id            string `json:"Id" schema:"Id"`
	Attempt       int    `json:"Attempt" schema:"Attempt"`
	Notifications string `json:"name" schema:"name"`
}

type KiotVietCustomer struct {
	Id            int       `json:"Id" schema:"Id"`
	Code          string    `json:"Code" schema:"Code"`
	Name          string    `json:"Name" schema:"Name"`
	ContactNumber string    `json:"ContactNumber" schema:"ContactNumber"`
	Address       string    `json:"Address" schema:"Address"`
	LocationName  string    `json:"LocationName" schema:"LocationName"`
	ModifiedDate  time.Time `json:"ModifiedDate" schema:"ModifiedDate"`
	Type          int       `json:"Type" schema:"Type"`
	Organization  string    `json:"Organization" schema:"Organization"`
	BranchId      int       `json:"BranchId" schema:"BranchId"`
}

type HookKiotVietNotificationData struct {
	Action string        `json:"Action" schema:"Action"`
	Data   []interface{} `json:"Data" schema:"Data"`
}

type HookKiotVietNotifications struct {
	Id            string                         `json:"Id" schema:"Id"`
	Attempt       int                            `json:"Attempt" schema:"Attempt"`
	Notifications []HookKiotVietNotificationData `json:"Notifications" schema:"Notifications"`
}

type KiotVietInvoice struct {
	Type            string    `json:"__type"`
	Id              int       `json:"Id"`
	Code            string    `json:"Code"`
	PurchaseDate    time.Time `json:"PurchaseDate"`
	BranchId        int       `json:"BranchId"`
	BranchName      string    `json:"BranchName"`
	SoldById        int       `json:"SoldById"`
	SoldByName      string    `json:"SoldByName"`
	CustomerId      int       `json:"CustomerId"`
	CustomerCode    string    `json:"CustomerCode"`
	CustomerName    string    `json:"CustomerName"`
	Total           int       `json:"Total"`
	TotalPayment    int       `json:"TotalPayment"`
	Status          int       `json:"Status"`
	StatusValue     string    `json:"StatusValue"`
	UsingCod        bool      `json:"UsingCod"`
	InvoiceDelivery struct {
		InvoiceId     int    `json:"InvoiceId"`
		ServiceType   string `json:"ServiceType"`
		Status        int    `json:"Status"`
		Receiver      string `json:"Receiver"`
		ContactNumber string `json:"ContactNumber"`
		Address       string `json:"Address"`
		LocationName  string `json:"LocationName"`
		Weight        int    `json:"Weight"`
		Length        int    `json:"Length"`
		Width         int    `json:"Width"`
		Height        int    `json:"Height"`
		UsingPriceCod bool   `json:"UsingPriceCod"`
	} `json:"InvoiceDelivery"`
	InvoiceDetails []struct {
		ProductId   int     `json:"ProductId"`
		ProductCode string  `json:"ProductCode"`
		ProductName string  `json:"ProductName"`
		Quantity    float32 `json:"Quantity"`
		Price       int     `json:"Price"`
		Discount    int     `json:"Discount"`
	} `json:"InvoiceDetails"`
	Payments []struct {
		Id        int       `json:"Id"`
		Code      string    `json:"Code"`
		Amount    int       `json:"Amount"`
		Method    string    `json:"Method"`
		Status    int       `json:"Status"`
		TransDate time.Time `json:"TransDate"`
	} `json:"Payments"`
}
