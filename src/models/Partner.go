package models

import "shopf1/go-flow/entities"

type Action struct {
	ActionType string `json:"ActionType" schema:"ActionType"`
	ActionId   string `json:"ActionId" schema:"ActionId"`
}

type Condition struct {
	Field   string   `json:"Field" schema:"Field"`
	Symbols string   `json:"Symbols" schema:"Symbols"`
	Value   string   `json:"Value" schema:"Value"`
	Actions []Action `json:"Actions" schema:"Actions"`
}

type Config struct {
	Triggers []Condition `json:"Triggers" schema:"Triggers"`
}

type PartnerParse struct {
	entities.Partners
	ConfigParse Config
}

type PartnerMessage struct {
	Type string      `json:"type" schema:"type"`
	Data interface{} `json:"data" schema:"data"`
}
