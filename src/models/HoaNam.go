package models

import "time"

type HoaNamData struct {
	Id            int       `json:"id" schema:"id"`
	Code          string    `json:"code" schema:"code"`
	Name          string    `json:"name" schema:"name"`
	Phone         string    `json:"phone" schema:"phone"`
	Address       string    `json:"address" schema:"address"`
	CreatedAt     time.Time `json:"created_at" schema:"created_at"`
	UpdatedAt     time.Time `json:"updated_at" schema:"updated_at"`
	Bill          string    `json:"bill" schema:"bill"`
	RMBToVND      string    `json:"rmb_to_vnd" schema:"rmb_to_vnd"`
	CostChina1VND int       `json:"cost_china1_vnd" schema:"cost_china1_vnd"`
	CostChina2VND int       `json:"cost_china2_vnd" schema:"cost_china2_vnd"`
	CostVietNam   string    `json:"cost_vietnam" schema:"cost_vietnam"`
	Revenue       int       `json:"revenue" schema:"revenue"`
	Paid          int       `json:"paid" schema:"paid"`
	Debt          int       `json:"debt" schema:"debt"`
	StatusText    string    `json:"status_text" schema:"status_text"`
}
