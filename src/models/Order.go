package models

import (
	"shopf1/go-flow/entities"
	"time"
)

type CreateOrderRequest struct {
	ID           int    `json:"id" schema:"id"`
	ShopID       int    `json:"shop_id" schema:"shop_id"`
	OrderNumber  string `json:"order_number" schema:"order_number"`
	ShippingCode string `json:"shipping_code" schema:"shipping_code"`
	Status       int    `json:"status" schema:"status"` // 0 - new, 1 - Đã xác nhận, 2 - chờ lấy hàng, 3 - đã lấy hàng, 7 - đang giao hàng, 8 - đề nghị hoàn, 6 - đang hoàn hàng, -1 - đã hủy, 5 - hoàn thành công, 4 - giao thành công
	CustomerID   int    `json:"customer_id" schema:"customer_id"`

	StockID                 int                     `json:"stock_id" schema:"stock_id"`
	StockAddress            string                  `json:"stock_address" schema:"stock_address"`
	ShippingMethod          string                  `json:"shipping_method" schema:"shipping_method"`
	ShippingName            string                  `json:"shipping_name" schema:"shipping_name"`
	ShippingPhone           string                  `json:"shipping_phone" schema:"shipping_phone"`
	ShippingAddress         string                  `json:"shipping_address" schema:"shipping_address"`
	ShippingDistrictID      int                     `json:"shipping_district_id" schema:"shipping_district_id"`
	ShippingProvinceID      int                     `json:"shipping_province_id" schema:"shipping_province_id"`
	ShippingDistrict        string                  `json:"shipping_district" schema:"shipping_district"`
	EstimateShipDate        *time.Time              `json:"estimate_ship_date,omitempty" schema:"estimate_ship_date"`
	ShippingStatus          string                  `json:"shipping_status" schema:"shipping_status"`
	ShippingStatusName      string                  `json:"shipping_status_name" schema:"shipping_status_name"`
	Weight                  int                     `json:"weight" schema:"weight"`
	Quantity                int                     `json:"quantity" schema:"quantity"`
	ShipRevenue             int                     `json:"ship_revenue" schema:"ship_revenue"` // Thu ship khách hàng
	Discount                int                     `json:"discount" schema:"discount"`
	Prepaid                 int                     `json:"prepaid" schema:"prepaid"`
	Total                   int                     `json:"total" schema:"total"`
	CodAmount               int                     `json:"cod_amount" schema:"cod_amount"`
	PurchaseCost            int                     `json:"purchase_cost" schema:"purchase_cost"`
	Note                    string                  `json:"note" schema:"note"`
	NoteBill                string                  `json:"note_bill" schema:"note_bill"`
	PaymentMethod           int                     `json:"payment_method" schema:"payment_method"`
	ChannelID               int                     `json:"channel_id" schema:"channel_id"`
	ShipFee                 int                     `json:"ship_fee" schema:"ship_fee"`                   // Phí ship
	ReturnFee               int                     `json:"return_fee" schema:"return_fee"`               // Cước chuyển hoàn
	ShippedDate             *time.Time              `json:"shipped_date,omitempty" schema:"shipped_date"` // Ngày lấy đơn hàng
	PickupDate              *time.Time              `json:"pickup_date,omitempty" schema:"pickup_date"`   // Ngày đơn vị vận chuyển lấy hàng
	RevertDate              *time.Time              `json:"revert_date,omitempty" schema:"revert_date"`   // Ngày hoàn đơn hàng
	GoodsCostTotal          int                     `json:"goods_cost_total" schema:"goods_cost_total"`   // Giá vốn của đơn = tổng giá vốn của các item trong đơn
	PageID                  string                  `json:"page_id" schema:"page_id"`                     // Page bán hàng
	PostID                  string                  `json:"post_id" schema:"post_id"`                     // Post bán hàng
	Detail                  string                  `json:"detail" schema:"detail"`                       // Diễn giải
	CreatedBy               int                     `json:"created_by" schema:"created_by"`
	CreatedAt               time.Time               `json:"created_at" schema:"created_at"`
	UpdatedAt               time.Time               `json:"updated_at" schema:"updated_at"`
	StatusDate              *time.Time              `json:"status_date,omitempty" schema:"status_date"`
	Chanel                  string                  `json:"chanel" schema:"chanel"`                     // Kênh: FB, ZL, INTA
	ShipPaidStatus          int                     `json:"ship_paid_status" schema:"ship_paid_status"` // Trạng thái thanh toán với bên vận chuyển: 0 chua thanh toan, 1: doi thanh toan, 2 dax thanh toan
	ConfirmReturn           int                     `json:"confirm_return" schema:"confirm_return"`     // 0: chua xac nhan hoan, chua cap nha kho 1: da xac nhan hoan, da day hang lai kho
	PaidDate                *time.Time              `json:"paid_date,omitempty" schema:"paid_date"`
	ConfirmDate             *time.Time              `json:"confirm_date,omitempty" schema:"confirm_date"`
	InputStock              int                     `json:"input_stock" schema:"input_stock"` // 0: chưa nhập kho || 1: đơn nhận một phần || 2: đơn bảo hành
	SendMessageStatusChange int                     `json:"send_message_status_change" schema:"send_message_status_change"`
	ShippingWardID          int                     `json:"shipping_ward_id" schema:"shipping_ward_id"`
	Products                []entities.ProductStock `json:"products" schema:"products"`
	SocialId                string                  `json:"social_id" schema:"social_id"`
	AdsId                   string                  `json:"ads_id" schema:"ads_id"`
}
