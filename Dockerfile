FROM golang:1.17

WORKDIR /app
COPY ./partner-sync ./partner-sync
COPY ./flow-go ./flow-go
WORKDIR /app/flow-go
RUN go get -d -v ./...
RUN go install -v ./...
WORKDIR /app/partner-sync
RUN go get -d -v ./...
RUN go install -v ./...
RUN go build -o server

EXPOSE 3000

CMD ["./server"]
