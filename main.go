package main

import (
	"common/database"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
	"partner-sync/src/handle"
	"partner-sync/src/middlewares"
	"partner-sync/src/routes"
	"shopf1/go-flow/utils"
)

func main() {
	utils.InitENV()
	database.InitMysql()
	router := mux.NewRouter()

	router.NotFoundHandler = http.HandlerFunc(middlewares.NotFoundHandle)
	router.MethodNotAllowedHandler = http.HandlerFunc(middlewares.MethodNotAllowed)
	router.Use(middlewares.Handle)
	routes.Handle(router)
	err := godotenv.Load()
	if err != nil {
		fmt.Println(err)
	}
	//entities.InitRedis()
	//jobs.StartJob()
	handle.ChuyenShop()
	go handle.InitConsumer()

	name, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	fmt.Printf("host name: %v", name)

	corsObj := handlers.AllowedOrigins([]string{"*"})

	err = http.ListenAndServe("0.0.0.0:3000", handlers.CORS(corsObj)(router))
	if err != nil {
		log.Fatalf("ListenAndServe error: %s", err)
	}
}
